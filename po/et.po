# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# 
# Translators:
# Ivar Smolin <okul@linux.ee>, 2014-2015
# Ivar Smolin <okul@linux.ee>, 2009, 2010
# Lauris Kaplinski <lauris@ariman.ee>, 1999
# Priit Laes <amd store20 com>, 2005
# Tõivo Leedjärv <toivo@linux.ee>, 2002, 2003
msgid ""
msgstr ""
"Project-Id-Version: MATE Desktop Environment\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2015-12-14 14:07+0300\n"
"PO-Revision-Date: 2015-09-13 19:30+0000\n"
"Last-Translator: Ivar Smolin <okul@linux.ee>\n"
"Language-Team: Estonian (http://www.transifex.com/mate/MATE/language/et/)\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Language: et\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#: ../data/mate-volume-control.desktop.in.in.h:1
msgid "Sound"
msgstr "Heli"

#: ../data/mate-volume-control.desktop.in.in.h:2
msgid "Change sound volume and sound events"
msgstr "Helitugevuse muutmine ja sündmuste jaoks helide määramine"

#: ../data/mate-volume-control-applet.desktop.in.h:1
#: ../mate-volume-control/dialog-main.c:245
#: ../mate-volume-control/gvc-channel-bar.c:500
#: ../mate-volume-control/gvc-speaker-test.c:461
msgid "Volume Control"
msgstr "Helitugevuse regulaator"

#: ../data/mate-volume-control-applet.desktop.in.h:2
msgid "Show desktop volume control"
msgstr "Töölaua helitugevuse regulaatori näitamine"

#. Translators: This is the name of an audio file that sounds like the bark of
#. a dog.
#. You might want to translate it into the equivalent words of your language.
#: ../data/sounds/mate-sounds-default.xml.in.in.h:3
msgid "Bark"
msgstr "Haukumine"

#. Translators: This is the name of an audio file that sounds like a water
#. drip.
#. You might want to translate it into the equivalent words of your language.
#: ../data/sounds/mate-sounds-default.xml.in.in.h:6
msgid "Drip"
msgstr "Tilkumine"

#. Translators: This is the name of an audio file that sounds like tapping
#. glass.
#. You might want to translate it into the equivalent words of your language.
#: ../data/sounds/mate-sounds-default.xml.in.in.h:9
msgid "Glass"
msgstr "Klaas"

#. Translators: This is the name of an audio file that sounds sort of like a
#. submarine sonar ping.
#. You might want to translate it into the equivalent words of your language.
#: ../data/sounds/mate-sounds-default.xml.in.in.h:12
msgid "Sonar"
msgstr "Sonar"

#: ../mate-volume-control/applet-main.c:45
#: ../mate-volume-control/dialog-main.c:187
msgid "Version of this application"
msgstr "Selle rakenduse versioon"

#: ../mate-volume-control/applet-main.c:46
#: ../mate-volume-control/dialog-main.c:185
msgid "Enable debug"
msgstr "Luba silumine"

#: ../mate-volume-control/applet-main.c:55
msgid " — MATE Volume Control Applet"
msgstr " — MATE helitugevuse juhtrakend"

#: ../mate-volume-control/dialog-main.c:138
msgid "Sound system is not available"
msgstr "Helisüsteem pole saadaval"

#: ../mate-volume-control/dialog-main.c:160
msgid "Waiting for sound system to respond"
msgstr "Helisüsteemi vastuse ootamine"

#: ../mate-volume-control/dialog-main.c:184
msgid "Sound system backend"
msgstr "Helisüsteemi taustaprogramm"

#: ../mate-volume-control/dialog-main.c:186
msgid "Startup page"
msgstr "Käivitamisel aktiveeritav leht"

#: ../mate-volume-control/dialog-main.c:196
msgid " — MATE Volume Control"
msgstr " — MATE helitugevuse regulaator"

#: ../mate-volume-control/gvc-applet.c:322
#: ../mate-volume-control/gvc-mixer-dialog.c:2028
msgid "Input"
msgstr "Sisend"

#: ../mate-volume-control/gvc-applet.c:323
#: ../mate-volume-control/gvc-mixer-dialog.c:2130
msgid "Output"
msgstr "Väljund"

#: ../mate-volume-control/gvc-applet.c:326
msgid "Microphone Volume"
msgstr "Mikrofoni valjus"

#: ../mate-volume-control/gvc-applet.c:328
msgid "Sound Output Volume"
msgstr "Heliväljundi valjus"

#: ../mate-volume-control/gvc-applet.c:333
msgid "MATE Volume Control Applet"
msgstr "MATE helitugevuse juhtimise rakend"

#: ../mate-volume-control/gvc-balance-bar.c:158
msgctxt "balance"
msgid "Left"
msgstr "Vasak"

#: ../mate-volume-control/gvc-balance-bar.c:159
msgctxt "balance"
msgid "Right"
msgstr "Parem"

#: ../mate-volume-control/gvc-balance-bar.c:162
msgctxt "balance"
msgid "Rear"
msgstr "Tagumine"

#: ../mate-volume-control/gvc-balance-bar.c:163
msgctxt "balance"
msgid "Front"
msgstr "Eesmine"

#: ../mate-volume-control/gvc-balance-bar.c:166
msgctxt "balance"
msgid "Minimum"
msgstr "Väikseim"

#: ../mate-volume-control/gvc-balance-bar.c:167
msgctxt "balance"
msgid "Maximum"
msgstr "Suurim"

#: ../mate-volume-control/gvc-balance-bar.c:355
msgid "_Balance:"
msgstr "_Tasakaal:"

#: ../mate-volume-control/gvc-balance-bar.c:358
msgid "_Fade:"
msgstr "_Tuhmumine:"

#: ../mate-volume-control/gvc-balance-bar.c:361
msgid "_Subwoofer:"
msgstr "Madal_sageduskõlar:"

#: ../mate-volume-control/gvc-channel-bar.c:330
msgctxt "volume"
msgid "Unamplified"
msgstr "Võimendamata"

#: ../mate-volume-control/gvc-channel-bar.c:343
msgctxt "volume"
msgid "100%"
msgstr "100%"

#. Mute button
#: ../mate-volume-control/gvc-channel-bar.c:1099
msgid "Mute"
msgstr "Tumm"

#: ../mate-volume-control/gvc-combo-box.c:257
#: ../mate-volume-control/gvc-mixer-dialog.c:1617
msgid "_Profile:"
msgstr "_Profiil:"

#: ../mate-volume-control/gvc-mixer-dialog.c:355
#: ../mate-volume-control/gvc-mixer-dialog.c:504
msgid "Co_nnector:"
msgstr "_Pistik:"

#. translators:
#. * The device has been disabled
#: ../mate-volume-control/gvc-mixer-dialog.c:1126
msgid "Disabled"
msgstr "Keelatud"

#. translators:
#. * The number of sound outputs on a particular device
#: ../mate-volume-control/gvc-mixer-dialog.c:1132
#, c-format
msgid "%u Output"
msgid_plural "%u Outputs"
msgstr[0] "%u väljund"
msgstr[1] "%u väljundit"

#. translators:
#. * The number of sound inputs on a particular device
#: ../mate-volume-control/gvc-mixer-dialog.c:1141
#, c-format
msgid "%u Input"
msgid_plural "%u Inputs"
msgstr[0] "%u sisend"
msgstr[1] "%u sisendit"

#: ../mate-volume-control/gvc-mixer-dialog.c:1508
#: ../mate-volume-control/gvc-mixer-dialog.c:1777
#: ../mate-volume-control/gvc-sound-theme-chooser.c:872
msgid "Name"
msgstr "Nimi"

#: ../mate-volume-control/gvc-mixer-dialog.c:1556
#, c-format
msgid "Speaker Testing for %s"
msgstr "%s kõlarite testimine"

#: ../mate-volume-control/gvc-mixer-dialog.c:1620
msgid "Test Speakers"
msgstr "Kõlarite test"

#: ../mate-volume-control/gvc-mixer-dialog.c:1829
msgid "Sound Effects"
msgstr "Heliefektid"

#: ../mate-volume-control/gvc-mixer-dialog.c:1852
msgid "_Alert volume: "
msgstr "_Hoiatuste valjus: "

#: ../mate-volume-control/gvc-mixer-dialog.c:1927
msgid "_Output volume: "
msgstr "_Väljundi valjus: "

#: ../mate-volume-control/gvc-mixer-dialog.c:1974
msgid "Hardware"
msgstr "Riistvara"

#: ../mate-volume-control/gvc-mixer-dialog.c:1979
msgid "C_hoose a device to configure:"
msgstr "_Seadistatava seadme valimine:"

#: ../mate-volume-control/gvc-mixer-dialog.c:2006
#: ../mate-volume-control/gvc-mixer-dialog.c:2162
msgid "Settings for the selected device:"
msgstr "Valitud seadme sätted:"

#: ../mate-volume-control/gvc-mixer-dialog.c:2036
msgid "_Input volume: "
msgstr "_Sisendi valjus: "

#: ../mate-volume-control/gvc-mixer-dialog.c:2070
msgid "Input level:"
msgstr "Sisendi valjus:"

#: ../mate-volume-control/gvc-mixer-dialog.c:2099
msgid "C_hoose a device for sound input:"
msgstr "H_elisisendi seadme valimine:"

#: ../mate-volume-control/gvc-mixer-dialog.c:2135
msgid "C_hoose a device for sound output:"
msgstr "_Heliväljundi seadme valimine:"

#: ../mate-volume-control/gvc-mixer-dialog.c:2196
msgid "Applications"
msgstr "Rakendused"

#: ../mate-volume-control/gvc-mixer-dialog.c:2201
msgid "No application is currently playing or recording audio."
msgstr "Ükski rakendus ei esita ega salvesta hetkel audiot."

#: ../mate-volume-control/gvc-mixer-dialog.c:2388
#: ../mate-volume-control/gvc-sound-theme-chooser.c:736
#: ../mate-volume-control/gvc-sound-theme-chooser.c:748
#: ../mate-volume-control/gvc-sound-theme-chooser.c:760
msgid "Sound Preferences"
msgstr "Helieelistused"

#: ../mate-volume-control/gvc-sound-theme-chooser.c:324
msgid "No sounds"
msgstr "Helid puuduvad"

#: ../mate-volume-control/gvc-sound-theme-chooser.c:445
msgid "Built-in"
msgstr "Sisseehitatud"

#: ../mate-volume-control/gvc-sound-theme-chooser.c:632
#: ../mate-volume-control/sound-theme-file-utils.c:292
msgid "Custom"
msgstr "Kohandatud"

#: ../mate-volume-control/gvc-sound-theme-chooser.c:739
#: ../mate-volume-control/gvc-sound-theme-chooser.c:750
#: ../mate-volume-control/gvc-sound-theme-chooser.c:762
msgid "Testing event sound"
msgstr "Sündmuse heli testimine"

#: ../mate-volume-control/gvc-sound-theme-chooser.c:848
msgid "Default"
msgstr "Vaikimisi"

#: ../mate-volume-control/gvc-sound-theme-chooser.c:849
msgid "From theme"
msgstr "Teemast"

#: ../mate-volume-control/gvc-sound-theme-chooser.c:879
msgid "Type"
msgstr "Liik"

#: ../mate-volume-control/gvc-sound-theme-chooser.c:1064
msgid "Sound _theme:"
msgstr "Heli_teema:"

#: ../mate-volume-control/gvc-sound-theme-chooser.c:1077
msgid "C_hoose an alert sound:"
msgstr "H_oiatuse heli valimine:"

#: ../mate-volume-control/gvc-sound-theme-chooser.c:1109
msgid "Enable _window and button sounds"
msgstr "_Akende ja nuppude helid on lubatud"

#: ../mate-volume-control/gvc-speaker-test.c:262
msgid "Stop"
msgstr "Peata"

#: ../mate-volume-control/gvc-speaker-test.c:262
#: ../mate-volume-control/gvc-speaker-test.c:380
msgid "Test"
msgstr "Testi"

#: ../mate-volume-control/gvc-stream-status-icon.c:232
#, c-format
msgid "Failed to start Sound Preferences: %s"
msgstr "Tõrge helieelistuste käivitamisel: %s"

#: ../mate-volume-control/gvc-stream-status-icon.c:267
msgid "_Mute"
msgstr "_Tumm"

#: ../mate-volume-control/gvc-stream-status-icon.c:278
#: ../mate-volume-control/gvc-stream-status-icon.c:280
msgid "_Sound Preferences"
msgstr "_Helieelistused"

#: ../mate-volume-control/gvc-stream-status-icon.c:475
msgid "Muted"
msgstr "Tumm"

#: ../mate-volume-control/gvc-utils.c:59
msgid "Unknown"
msgstr "Tundmatu"

#. Speaker channel names
#: ../mate-volume-control/gvc-utils.c:61
#: ../mate-volume-control/gvc-utils.c:109
msgid "Mono"
msgstr "Mono"

#: ../mate-volume-control/gvc-utils.c:62
msgid "Front Left"
msgstr "Eesmine vasak"

#: ../mate-volume-control/gvc-utils.c:63
msgid "Front Right"
msgstr "Eesmine parem"

#: ../mate-volume-control/gvc-utils.c:64
msgid "Front Center"
msgstr "Eesmine keskmine"

#: ../mate-volume-control/gvc-utils.c:65
msgid "LFE"
msgstr ""

#: ../mate-volume-control/gvc-utils.c:66
msgid "Rear Left"
msgstr "Tagumine vasak"

#: ../mate-volume-control/gvc-utils.c:67
msgid "Rear Right"
msgstr "Tagumine parem"

#: ../mate-volume-control/gvc-utils.c:68
msgid "Rear Center"
msgstr "Tagumine keskmine"

#: ../mate-volume-control/gvc-utils.c:69
msgid "Front Left of Center"
msgstr ""

#: ../mate-volume-control/gvc-utils.c:70
msgid "Front Right of Center"
msgstr ""

#: ../mate-volume-control/gvc-utils.c:71
msgid "Side Left"
msgstr ""

#: ../mate-volume-control/gvc-utils.c:72
msgid "Side Right"
msgstr ""

#: ../mate-volume-control/gvc-utils.c:73
msgid "Top Front Left"
msgstr ""

#: ../mate-volume-control/gvc-utils.c:74
msgid "Top Front Right"
msgstr ""

#: ../mate-volume-control/gvc-utils.c:75
msgid "Top Front Center"
msgstr ""

#: ../mate-volume-control/gvc-utils.c:76
msgid "Top Center"
msgstr ""

#: ../mate-volume-control/gvc-utils.c:77
msgid "Top Rear Left"
msgstr ""

#: ../mate-volume-control/gvc-utils.c:78
msgid "Top Rear Right"
msgstr ""

#: ../mate-volume-control/gvc-utils.c:79
msgid "Top Rear Center"
msgstr ""

#: ../mate-volume-control/gvc-utils.c:114
msgid "Stereo"
msgstr "Stereo"

#: ../mate-volume-control/gvc-utils.c:121
msgid "Surround 4.0"
msgstr "Surround 4.0"

#: ../mate-volume-control/gvc-utils.c:129
msgid "Surround 4.1"
msgstr "Surround 4.1"

#: ../mate-volume-control/gvc-utils.c:131
msgid "Surround 5.0"
msgstr "Surround 5.0"

#: ../mate-volume-control/gvc-utils.c:141
msgid "Surround 5.1"
msgstr "Surround 5.1"

#: ../mate-volume-control/gvc-utils.c:152
msgid "Surround 7.1"
msgstr "Surround 7.1"
