mate-media (1.14.1-1+devuan) jessie-proposed-updates; urgency=medium

  * update

 -- Antonio Volpicelli <nioanto@live.it>  Wed, 10 May 2017 23:10:19 +0200

mate-media (1.14.1-1) unstable; urgency=medium

  [ Martin Wimpress ]
  * New upstream release.

  [ Vangelis Mouhtsis ]
  * debian/{control,rules}:
    + dbgsym: Don't build dbg:packages anymore.
      See https://wiki.debian.org/AutomaticDebugPackages.

 -- Mike Gabriel <sunweaver@debian.org>  Mon, 22 Aug 2016 11:55:55 +0200

mate-media (1.14.0-1) unstable; urgency=medium

  [ Vangelis Mouhtsis ]
  * debian/control:
    + Versioned B-D on dpkg-dev (>= 1.16.1.1).
    + Use encrypted URLs for Vcs-*: field.
  * debian/rules:
    + Enable all hardening flags.

  [ Martin Wimpress ]
  * New upstream release.
  * debian/control:
    + B-D: libcanberra-gtk3-dev
    + B-D: libgtk-3-dev
    + Versioned B-D: libmate-desktop-dev (>= 1.14)
    + Versioned B-D: libmatemixer-dev (>= 1.14)
    + B-D: libunique-3.0-dev
    + Versioned B-D: mate-common (>= 1.14)
    + Versioned D (mate-media): mate-desktop-common (>= 1.14).
  * debian/rules:
    + Add --with-gtk=3.0

  [ Mike Gabriel ]
  * debian/control:
    + Bump Standards: to 3.9.8. No changes needed.

 -- Mike Gabriel <sunweaver@debian.org>  Wed, 25 May 2016 16:26:57 +0200

mate-media (1.12.1-1) unstable; urgency=medium

  [ Martin Wimpress ]
  * New upstream release.
  * debian/control:
    + Versioned B-D: mate-common (>= 1.12), libmate-desktop-dev (>= 1.12) and
      libmatemixer-dev (>= 1.12).

 -- Mike Gabriel <sunweaver@debian.org>  Sat, 02 Jan 2016 07:36:57 +0100

mate-media (1.10.0-2) unstable; urgency=medium

  * Re-upload to unstable to sort out a recent auto-decrufter code bug.
    (Closes: #800950).

 -- Mike Gabriel <sunweaver@debian.org>  Wed, 07 Oct 2015 10:30:26 +0200

mate-media (1.10.0-1) unstable; urgency=medium

  [ Martin Wimpress ]
  * debian/rules:
    + Add missing line continuation.

  [ Mike Gabriel ]
  * Upload to unstable.

 -- Mike Gabriel <sunweaver@debian.org>  Mon, 24 Aug 2015 11:46:59 +0200

mate-media (1.10.0-1~exp1) experimental; urgency=medium

  [ Martin Wimpress ]
  * New upstream release.
    - Completely drop GStreamer support. (Closes: 768101, #785850).
  * debian/rules:
    + Switch to pure debhelper.
    + Use simple uscan rule to obtain orig tarball. Upstream
      tarball is now DFSG-compliant.
  * debian/watch:
    + Drop dversionmangle option.
  * debian/control:
    + Replace obsolete bin:packages mate-media-gstreamer and
      mate-media-pulseaudio with bin:package mate-media.
  * debian/copyright:
    + Update for new upstream release.
  * debian/patches:
    + Drop 1001_add-keywords-key-to-desktop-files.patch. Applied upstream.
    + Drop 1002_glib_connect_first.patch. Applied upstream.
    + Drop 2001_omit-gfdl-licensed-help-files.patch. Upstream is now
      DFSG compliant.
  * debian/mate-media-gstreamer.install:
    + Replace with mate-media.install.
  * debian/control:
    + Processed with wrap-and-sort.

  [ Mike Gabriel ]
  * debian/control:
    + Versioned B-D: mate-common (>= 1.10).
    + Mention libmatemixer in LONG_DESCRIPTION of bin:package mate-media.
  * debian/copyright:
    + Update to latest upstream release.

 -- Mike Gabriel <sunweaver@debian.org>  Wed, 17 Jun 2015 14:09:04 +0200

mate-media (1.8.0+dfsg1-4) unstable; urgency=medium

  * debian/patches:
    + Add 1002_glib_connect_first.patch. GLib >= 2.43 compatibility patch.
      (Closes: #780780).
  * debian/control:
    + Bump Standards: to 3.9.6. No changes needed.
  * debian/copyright:
    + Really mention all files in copyright file. Esp. list copyright
      holders of the po/*.po files.

 -- Mike Gabriel <sunweaver@debian.org>  Thu, 21 May 2015 09:32:14 +0200

mate-media (1.8.0+dfsg1-3) unstable; urgency=medium

  * debian/control:
    + Prefer pulse backend of MATE media to the gstreamer backend on default
      installations. (Closes: #753584).
    + Fix EOLs for several multi-line fields.

 -- Mike Gabriel <sunweaver@debian.org>  Mon, 04 Aug 2014 18:10:10 +0200

mate-media (1.8.0+dfsg1-2) unstable; urgency=low

  [ Vangelis Mouhtsis ]
  * debian/rules:
    + Replace on dh_install --list-missing -> --fail-missing.
    + Remove non-packaged *.convert files after build.

  [ Mike Gabriel ]
  * debian/control:
    + Fix D (bin:package mate-media-gstreamer): marco-common (instead of
      marco >= 1.5.0). (Closes: #750456).

 -- Mike Gabriel <sunweaver@debian.org>  Wed, 04 Jun 2014 01:44:40 +0200

mate-media (1.8.0+dfsg1-1) unstable; urgency=low

  * Initial release. (Closes: #734802).

 -- Mike Gabriel <sunweaver@debian.org>  Tue, 22 Apr 2014 21:54:28 +0200
